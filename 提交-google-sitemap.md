- 產生 sitemap 
    - 不是使用 static html 的網站都需要由開發者產生 sitemap 
    - 使用 static HTML 的網站可以用 https://www.xml-sitemaps.com/ 產生後請開發者上傳

- 請開發者將 xml 上傳至主機，並提供路徑
- 將 xml 路徑提供給客戶或由我們提交至 Google Search Console  
![image](uploads/cc7c476c60b91ed625a22b2e41c9778b/image.png)