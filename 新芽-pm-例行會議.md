## 每週專案例會
- PM 每週與 Alex、Thundersha、業務、設計、開發部門主管一起進行的會議
- 時間：每週一下午四點開始
- 會議前置作業
    - 會議前需先將各專案上週進度、本週預計進度及需討論的項目更新 25sprout/projects-wall> 上的各個 issue comment
    -   <details>
        <summary>展開閱讀 comment 需包含內容</summary>
        
        ## 1210

        ### 上週完成項目
        - APP/Web 本週已上架再改一版的 logo + Launch Page
        - APP/Web 本週預計做完 Landing Page 介紹內頁
        ### 本週規劃開發項目
        - 接下來預計要開發的項目：
          1.  第四階段首頁改版 UI 提案 -> 10 天
          1.  新增車輛流程變動 -> 8 天（建議客戶一次改完）
        ### 待討論
        - 預計開發項目客戶上週五有提到預計上線時間嗎？
        </details>

- 會議報告
    - 由每位 PM 分別報告專案進度，如有人力或技術問題也於個別報告時提出
    - 視會議情況，部分人力問題會待所有 PM 報告完後一併討論

- 會議記錄
    - 由 PM Head 指定的人選在 25sprout/projects-wall> 的 wiki 紀錄每次會議重大討論項目，例如人力在各專案間的配置、人力時程規劃等
    - 範例：
       - by 專案：https://fox.25sprout.com:10088/25sprout/projects-wall/wikis/20181008-pm-meeting-minute
       - by 人力：https://fox.25sprout.com:10088/25sprout/projects-wall/wikis/20181217-pm-meeting-minute
    - 需同時新增到 Wiki Home [PM 內部例會會議記錄](https://fox.25sprout.com:10088/25sprout/projects-wall/wikis/home#pm-%E5%85%A7%E9%83%A8%E4%BE%8B%E6%9C%83%E6%9C%83%E8%AD%B0%E8%A8%98%E9%8C%84) 方便查找

- 會議規則  
    - 遇到假日自動延至次個工作日相同時段
    - 若次個工作日相同時段有會議則動態調整至該日期他時間
    - 若有同仁 Remote 則以線上方式加入

## 每雙週 PM 例會
- 由 PM head 主持
- 與會者為 PM 及 PM 實習生
- 主要會先說明與討論新的專案流程文件、 PM team 內部作業文件等
- 偶爾會有公司 ~~政令宣導~~ 內務說明與討論 
- PM 若日常或特定專案有遇到什麼困難也可以在會議上提出，大家集思廣益

## 每月 Sprouter's Day
- 公司每月會固定舉辦新芽日
- PM 需報告負責的專案這一個月的進度以及接下來的時程規劃，大方向即可。例如：哪些功能開發中、預計什麼時候開發完/上線、測試中等等
    - 若是第一次講到的專案，大略簡述專案的內容、比較特別要做的功能等
    - 若專案已經上線可以 demo 分享比較特別的功能給大家看


## Quarterly Review
- 每季舉行一次針對上一季專案情形的討論，會議內容請見 [Project_BU_Quarterly_Meeting_Guideline.key](uploads/4e8ff67b2fa2bdef1d82b5f90ef1d121/Project_BU_Quarterly_Meeting_Guideline.key)
- 時間：會在每季的第一個月由大家討論出 Quarterly Review 的時間
- PM 報告內容  
    - 這個季度完成 / 進行中的專案
    - 描述出該專案的 Key points（Good & Bad），如：
        - 加班情形嚴重，太晚補充人力
        - 答應客戶的時程太早，導致內部開發資源吃緊
        - 外包團隊不給力，交付的東西一直吃問題，對 UI 不細心
        - iOS Clyde 很給力，收費公道
    - 根據上述的檢討點，提出改善辦法建議，或是建議可以沿用至其他專案等



