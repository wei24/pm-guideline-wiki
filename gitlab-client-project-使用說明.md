## 範本  
https://fox.25sprout.com:10088/25sprout-project/template

## Client Project 建立流程
1. 新增 Client Project，名稱為 `客戶專案名稱_tickets`，例如 `CustomerPortal_tickets`
1. 請至 [MIS/accounts](https://fox.25sprout.com/mis/accounts) 申請建立客戶 GitLab 帳號（[範例 issue](https://fox.25sprout.com/mis/accounts/-/issues/1)），並由 PM 將帳號新增至客戶的 project，權限為 reporter 
2. 新增 issue 狀態的 label https://fox.25sprout.com:10088/25sprout-project/template/clientname/labels
![image](uploads/2bbb34d345d7dc293418b2789d3e0649/image.png)
3. 視專案情況，可以新增 milestone 或其他 label 供雙方溝通使用
4. 視專案情況，可以新增一個 bug issue 作為範本，供客戶填寫 bug issue 時參考需提供的資料

## 使用流程概要

常見順序：1->2->3->4->5->6 或 1->2->3->4->7

1. 新增 issue：通常由客戶進行發起並 assign PM，但也可能是 PM 將客戶在其他聯絡管道提出的事項新增至 GitLab 做後續追蹤，或 PM 要跟客戶確認項目也可以在此 GitLab project 進行。
2. PM 收到 issue 後回覆：PM 收到 issue 後回覆客戶已收到、內部已經開始處理 `label:Doing`，或是有後續規格/文案等要跟客戶確認 `label:Planning`，若有與客戶討論過完成的時間，亦可使用 due date。
3. 25sprout 更新/修正：新增 25sprout 內部 project issue，並於內文貼上關聯的客戶端 issue ，例如：`rel:https://fox.25sprout.com:10088/25sprout-project/template/clientname/issues/1`，後續根據內部既有流程進行。
4. 客戶確認：內部完成後，由 PM 至 issue 留言，並附上完成項目的相關截圖、連結，將 assignee 換為開立 issue 的客戶、請客戶確認。 `label:Reviewing by User`
5. 更新至 Stg/Production 主機：客戶確認後，根據 Issue 及各專案狀態，可能會上到 staging 或 production。 `label:Ready for Production` 或是 `Ready for Staging`，可以請客戶 review 沒問題後更換 label 並將 assignee 改為 PM，但如果客戶僅留言、沒有變動 label /更換 assignee，需由 PM 更換 label 及 assignee。
6. 更新完成：PM 請開發者更新至 staging 或 production 後，留言通知客戶、更新 label，同時關閉 issue。`label: On Production` 或是 `On Staging`
7. 若還只有在 dev 的專案，客戶確認後，可以請客戶換 label 及關閉 issue 或由 PM 自己進行。 `label:Checked on Dev`


## 注意事項
1. 使用 GitLab 追蹤客戶需求或提出的 bug 基本上與 E-mail、Slack 各管道類似，但可以減少 PM 要從不同管道回覆客戶需求及處理進度的情形，同時客戶跟 PM 都可以快速追蹤目前代辦項目的數量、進度等。
2. 所有完成的 issue 狀態皆須為 closed 且 label 為 `On Production`/`On Staging`/`Checked on Dev`
3. 若客戶提出的需求很多，需請客戶排優先順序時，可以新增 label `High`/`Medium`/`Low` 供客戶標註用，但若數量較多需逐一排序時，亦可另外開 Google Sheet 將需求列出、請客戶排序