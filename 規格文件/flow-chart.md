## 目的
- 透過 flow chart 呈現功能的流程
- 需包含該流程內不同情境，例如是否已註冊、密碼是否正確、是否已付款等等

## 使用工具
- [draw.io](https://www.draw.io/)

## 範本

### 簡單版
- 只使用圓角矩形（頁面或使用者行為）及菱形（條件判斷）兩個圖示
- 圓角矩形為頁面或使用者行為  
![Kagulu_Flow_Chart_預約與賞車當日_POS_0210](uploads/c8fe013beb3aa943fdbc52472508daaf/Kagulu_Flow_Chart_預約與賞車當日_POS_0210.png)
- 部分功能 Flow Reference：此份資料僅供參考，每個站會需要依據實際情況調整https://drive.google.com/drive/folders/1wZ2F4JHI_n_Wm0weOcG3wCOyHwPrKGu7

### 完整版
- 包含多種圖形，並區分起始/截止點（橢圓）、頁面（矩形）、使用者行為（梯形）、菱形（決策）
![ABC-Mart_Flow_消費者下單流程_0817](uploads/da25294323528e1a740b0a9528f6752d/ABC-Mart_Flow_消費者下單流程_0817.png)

## 參考資料
- https://creately.com/diagram-type/objects/flowchart