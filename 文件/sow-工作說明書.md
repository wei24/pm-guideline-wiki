## Word 範本
https://drive.google.com/open?id=1CfpcYv_PZm89Vp-SHSmPemSg_jM9EQ5x

## 內容說明
https://docs.google.com/document/d/1mTLDtCkW9wFbpZ0uKiFonBTzTOujfyAglavgNHA1WEQ/edit#

### 格式
- 字體：正式文件，字體為標楷體（中）+ Times New Roman（英）

### 第一章 專案概述
- 更新 highlight 部分

### 第二章 專案管理說明
- 更新 highlight 部分

`2019/3/29 會議討論移除`
`### 第三章 網站規劃`
`- 描述重點功能`

### 第三章 系統架構圖
- 微調圖片內容

### 第四章 網站程式規劃 
- 專案功能、前端設計/開發項目、後端開發項目、BackStage 模組，基本上可以貼上報價單
- 可以選取報價單的 Job Items 和 Job Description 後，匯出為 PDF 後直接貼上（拖曳進 word）

### 第五章 工作時程規劃
- 貼上時程表：可以使用

### 第六章 專案組織與人力
- 依照內文更新專案參與人員，將同部門的人列在一起
- [人員職稱列表](https://docs.google.com/spreadsheets/d/1aGyJRV5DcEvWnCJYqGC6RAjRIlAjYWYoidMoDeuBNkI/edit#gid=0)

### 第七章 應交付資料清單與教育訓練
- 更新 highlight 部分

### 第八章 保固支援計畫
- 更新 highlight 部分
