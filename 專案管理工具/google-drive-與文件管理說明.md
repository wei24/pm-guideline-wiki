Google Drive Guideline

## 架構與資料夾名稱
![25sprout_Project_Flow-Google_Drive_架構](uploads/f0a17e6050e26035b7b314a59b9c5958/25sprout_Project_Flow-Google_Drive_架構.png)


### 設立新 Client/Project 資料夾
- 每次專案 Sales briefing 後，會將 project 資料夾移到該客戶資料夾內。若為新客戶則由 Sales 新增 client 資料夾後移動至 project 資料夾，由 PM 負責將該次專案資料夾結構調整為 Document 及相關資料
- client 及 project 的資料夾權限設定為開放給新芽網路所有人檢視  
![image](uploads/dde197a66746a4c1d9ea8dfbe8cfa440/image.png)

#### Document
- 單純 PM/Sales 使用的文件歸檔於此，例如報價單、合約、SOW、提案 Keynote 等，各檔案需包含原始檔、PDF 檔、雙方用印後的檔案（若有）
- 權限：關閉共用連結、單獨透過 e-mail 開放給 Slack channel 25jinli 成員可以編輯的權限  
![image](uploads/11bf156a335f88f6ee360cdd8e347047/image.png) 

#### 相關資料
- 分享給內部其他人員或客戶的文件歸檔於此，例如 UI 、flow chart、客戶提供的資料、時程表、規格使用到的文件等
- 權限：設定為開放給新芽網路所有人註解。編輯或需開放給外部瀏覽的身份另外透過共用設定，例如 UI 資料夾開放給設計師可以編輯、時程表開放給客戶瀏覽

## 文件命名規則

### 資料夾命名方式與代表意思
*  CE / 報價單
*  Contract / 合約
*  SOW / 工作說明書
*  NDA / 保密協定書
*  RFP / 需求建議書
*  Project Plan (PP) / 計畫說明書
*  Vendor Info (VI) / 廠商資料表  
*  Test Case / 測試項目
*  Weekly Meeting / 例會簡報或報告

其他文件皆使用中文名稱即可，例如測試文件、操作手冊等

### 檔案命名方式
文件的檔名由幾個部分組成－  
1.  文件類別 ex. CE, Contract, SOW  
1.  客戶名稱簡寫 ex. 緯創軟體, 遠傳, SEIKO
1.  專案名稱 ex. 雲端電子病例解決方案, 官網改版, 官方網站
1.  文件建立或最後一次編輯日期，格式 YYYYMMDD  
以上皆以英文底線_分隔  

範例：  
*  `CE_緯創軟體_雲端電子病歷解決方案_20160818 `
*  `CE_雲朗觀光_君品酒店官方網站_20160320`  
*  `Contract_SEIKO_Lukia產品網站_20160408`    
*  `Contract_遠傳_EMMA管理後台_20160129`  
*  `NDA_瓏山林_官方網站_20160103  `
*  `SOW_遠傳_SurveyCakeLite_20160927    `
*  `VI_25sprout_廠商資料表_20160218    `
