## GitLab Guideline
GitLab 為新芽內部專案管理主要使用之工具，亦使用於版本控制。PM 主要透過 GitLab Issue 將專案功能規格提供給開發者、安排開發進度，並使用 Wiki 記錄專案資訊與會議記錄。主機相關的帳號密碼資訊不存放於 GitLab


## 架構
![25sprout_Project_Flow-GitLab_架構](uploads/65c642959a37fd1a9626678c2d4191f7/25sprout_Project_Flow-GitLab_架構.png)

## 範本  
https://fox.25sprout.com:10088/25sprout-project/template

## 透過 SurveyCake 自動設定流程 GitLab
- 若客戶層 Group 已經存在，請確認已經被設定為該 Group 的 owner，沒有的話請通知 PM head 設定。若客戶層不存在，填寫問卷流程中可一併建立。
- 若為第一次做 SurveyCake Automation 的客戶，請先通知 PM head 開客戶層（需提供 sales 在 automation issue 的專案名稱）並將你設定為 owner

1. 根據內文說明填寫此份表單 https://25sprout.surveycake.com/s/bLGxK ，系統會自動將 Alex & Thundersha、Project BU 各部門主管、Sever Master(Suhu)、Cloud Manager(Peter)設為 owner
2. 若要新增的專案 subgroup 上層亦為 subgroup 且還未新增，需先開啟後才能透過表單新增此專案的 subgroup
3. Backend 的 project 多為自行 fork BackStage，因此此份問卷未列入
4. 正職設計師/開發者皆預設為 Maintainer，實習生皆預設為 Developer
5. 外部開發者仍需手動加入，請見下方 `手動設定流程` 第五點
6. 需手動更改 PM 的 project name 跟 path 為`PM_subgroupname`
7. 透過自動設定產生的專案 subgroup，會同步複製以下項目
    - [issue template](https://fox.25sprout.com:10088/25sprout-project/template/PM_template/issues)：會在開立 issue 時顯示  
![image](uploads/7ba752f126f3cffca8a29315ac2b238b/image.png)
    - [範例 labels](https://fox.25sprout.com:10088/groups/25sprout-project/-/labels)：可自行調整要使用的項目，非必備 label 可以刪除
    - 根據填答內容產生 milestone
8. Project 設定（Settings->General->Permissions）
    - 功能權限設定  
![image](uploads/6806f53f9be01064da0fdb5ac6c26445/image.png)
    - PM 關閉 Snippets 功能
    - 其他各部門 Project 關閉 Issues 功能
    - 若需使用 GitLab Pages 的 project 不能關閉 Repository 的功能

9. 若需追加 milestone 可填寫此份表單 https://25sprout.surveycake.com/s/GpeY4


## 手動設定流程
1. 若為新客戶或舊客戶未有 Group，由 PM 開新的 Group
    - Group 名稱設定為客戶名稱，基本上這個就會是網址路徑，不需另外調整，也因此名稱不能有空格。若有誤植請至 setting/general 一併更新 group name 跟 path
    - 權限：Private  
    - Member：Alex & Thundersha + Project BU 各部門主管 + Sever Master(Suhu)+ Cloud Manager(Peter)（Role Permission: Owner） 
    - 於 Group 內 Issues >> Labels 新增[此清單內的必備 label](https://fox.25sprout.com:10088/25sprout-project/guidelines/labels)，其他 label 視專案情況新增於 SubGroup

2. 若為已有客戶 Group 且 PM 未在該 Group 內
    - 由 PM Head 新增 PM（Role Permission: Owner）至 Group

3. 開新 Subgroup  
    - Subgroup 名稱設定為 `客戶名稱_專案名稱`，基本上這個就會是網址路徑，不需另外調整，也因此專案名稱不能有空格。若有誤植請至 setting/general 一併更新 project name 跟 path
    - Subgroup 權限設為 Private
    - Memeber 為專案所有內部成員  
            **Role Permission**  
              - 內部正職（不論人數）：Maintainer  
              - 內部實習生或兼職人員：Developer
    - 視專案預計 sprint 設定 [Milestone](https://fox.25sprout.com:10088/groups/25sprout-project/template/-/milestones/1/edit)，例如一週、兩週或四週。每個 milestone 需設定 Title, Start Date, Due Date
    - 視專案需求於 Subgroup 新增 label

4. 設定新 Project：包含 `PM_subgroupname` (PM 管理 issue 使用)、`Web`、`iOS`、`Android`（這三項是否新增取決於專案類型），後端通常會自己新增 Project，若沒有使用到 BackStage 需建立 `Backend` Project

5. Project 設定（Settings->General->Permissions）

    - 成員：若有外部開發者則新增到相關 Project，例如 `PM_subgroupname` & `Web`，若該專案全部外包，設定為 Maintainer 、有內部開發者一起，設定為 Developer 
    - 功能權限設定  
![image](uploads/6806f53f9be01064da0fdb5ac6c26445/image.png)
    - PM project 關閉 Snippets 功能
    - 其他各部門 project 關閉 Issues 功能
    - 若需使用 GitLab Pages 的 project 不能關閉 Repository 的功能
    - 如需新增外部開發者、客戶至 GitLab，請至 [MIS/accounts](https://fox.25sprout.com/mis/accounts) 申請，範例 [issue](https://fox.25sprout.com/mis/accounts/-/issues/1)


## Gitlab 使用基本原則
1. 所有部門的 Issue 都集中在同一個 Project，開發者可以透過部門 label 及 assignee 找到負責的 issue 
2. Feature Issue 分為 Feature 說明與各部門的（主要作為進度管理用），針對功能的討論或資訊皆集中在 Feature 說明的 issue 討論，基本上不在各部門的 issue 討論。Bug Issue 則在獨立 issue 討論，但若與 Feature 相關，PM 需負責拉回 Feature 討論。

3. Issue 種類與說明
- 目前共分為 Feauter、Task、Bug 三種 issue 
   - [Feature Issue 範例與說明](https://fox.25sprout.com:10088/25sprout-project/template/PM_template/issues/1)
        - Flow、Wireframe 兩個欄位主要為了讓 spec 可以更清楚的呈現，如果是比較單純或可以使用文字敘述完成的，不一定需要填寫這兩個欄位
        - PM 可以視各功能規模決定依照功能或頁面分為不同 feautre issue 

   - [各部門 Issue 範例與說明](https://fox.25sprout.com:10088/25sprout-project/template/PM_template/issues/2)
        - 各功能在各部門的 feature issue 可以視進度安排/追蹤需求，決定是否要將一個 feature issue 對應多個同部門 issue。例如購物車 feature 的金流與物流在後端因為拆在不同 milestone 做，可以分為金流的 issue 及物流的 issue。

   - [Task Issue 範例與說明](https://fox.25sprout.com:10088/25sprout-project/template/PM_template/issues/8)
        - Task issue 為開發完成後，若客戶有換圖換字、追加功能細節等需求，而 scope 又未大到為 feature 時，則屬於 task。

   - [Bug Issue 範例與說明](https://fox.25sprout.com:10088/25sprout-project/template/PM_template/issues/3)
        - 與規格不符或會導致在特定裝置無法使用等情情，歸屬於 bug issue

 - `20190302` 新增 Issue Template，填寫 SurveyCake 表單自動建立的 project 都可以看到範例並直接填寫內容  
![image](uploads/639887b4367f72712cb7f71a5da7ee7e/image.png)

3. 各部門的 code 都 push 到各自獨立的 Project
4. 開發者完成後直接關閉 issue，PM 測試完成後以 label :thumbsup: 標記
5. 各部門的 Feature Issue 完成後如果有需要調整的部分，直接開新的 Issue 以利追蹤
6. 非專案 PM 的測試者直接開 Bug Issue assign 給該專案負責 PM，由 PM 確認及補充後 assign 給開發者。後續若開發者對規格有問題，主要由負責 PM 回覆，若有特殊情況需由測試者回覆，會由 PM 提出。