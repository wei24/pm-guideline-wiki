## 會議目的
Kick-off Meeting 為專案正式開始前的會議，通常在客戶公司舉行。
會由 Sales 跟客戶敲定會議時間。  
當天由 Sales 將 PM 介紹給客戶，並由 PM 說明專案進行方式、專案重點功能及時程，通常也會有一些內部 Briefing 時討論到需要跟客戶確認的項目。    
同時需跟客戶詢問例會頻率，最好可以在當天將時間定下來。

## 與會人員
由 Sales 先與客戶確認客戶方與會人員，新芽必帶與會人員為該專案 Sales 及 PM，視專案情況以及當天預計討論的項目邀請開發者、設計師、Server Master、PM Head、Alex、Thundersha 等。  
例如：客戶 IT 會出席，視討論議題可能需要邀請開發者或 Server Master；若客戶老闆會出席，可能需要邀請 PM Head 或 Alex。

## 會議簡報內容 [範本與字體檔](https://drive.google.com/drive/folders/1syfoauH0nwGdaRAQ5yx6X6tDxHksa3m1)
字體  
- 中文：Noto Sans CJK TC
- 英文：Avenir

### 1. Goal
- 基本上可以直接使用 Sales 提案或 brief 的簡報，若無可移除此區塊（文字是否上色也以 sales 提供的文件為主）
- 報告時需將各項列點串成一段話，用不同的轉折詞銜接各點，而非照唸

### 2. 目標使用者
- 基本上可以直接使用 Sales 提案或 brief 的簡報，若無可移除此區塊
- 除了此頁簡報上的文字，需再以口述進一步說明目標使用者的樣貌

### 3. 專案說明
- 報告時主要與客戶說明專案流程、例會的討論重點，成員若沒有列席，則稍微提包含的部門即可。

    #### 3-1. 專案流程
    - 依照各專案實際情況更新，常見差異如下：
      1. 不一定會出 wireframe，如果確定不會可以移除
      2. 是否要串接資料庫：需加上`後端程式開發`及 `API 資料串接`
      3. 是否有 BackStage：需加上 `BackStage 開發`
      4. 報告時需說明 SIT 是新芽測試、 UAT 是客戶測試，可以背一下兩個名詞的全名

    #### 3-2. Scrum & Agile（敏捷式開發）
    - 基本上不變動
    - 報告時需讓客戶知道專案例會是簡短、迅速對焦的會議，並說明簡報上的會議內容，以及會議後都會寄發會議記錄 

    #### 3-3. 專案控管
    - 更新會議頻率
    - 報告時需特別強調需求變更
    - 若為雙週例會才需要第二點 `每週 E-mail 進度報告` 這項

    #### 3-4. 專案成員
    - 主要是讓客戶知道新芽有一個很完整的 team 在執行這個專案
    - 除了 CEO、CTO、PM head 之外，其他依照專案情況列出參與成員，若為外包則列該開發部門主管
    - [人員職稱列表](https://docs.google.com/spreadsheets/d/1aGyJRV5DcEvWnCJYqGC6RAjRIlAjYWYoidMoDeuBNkI/edit#gid=0)
    - 報告時若列表上的人有在現場，可以比一下他的方向，被提到的人跟客戶點頭示意。

### 4. 工作軟體與工具
- 預設為略過，若客戶有詢問相關主題再開這頁即可
- 報告時主要讓客戶知道我們是使用電話與 E-mail 溝通，其他項目稍微帶過，基本上不用逐項念，若客戶有提問再深入說明。

### 5. 專案時程
- 使用 [Keynote 時程表](https://drive.google.com/open?id=1aqc3N7tedHWmGDrQVIzE2IeMp3ti86XB)
- 若已有提供 Google Sheet 版本時程表給客戶，可以截圖並放連結在此頁，報告的這頁時直接開啟檔案討論即可

### 6. 專案功能架構
- 報告時主要功能稍微說明，以討論 sitemap 所列之頁面、內容及功能為主

    #### 6-1. 專案功能架構
    - 列出本站重點功能，可以參考報價單或 [projects-wall issue ticket 的內容](https://fox.25sprout.com:10088/25sprout/projects-wall/issues)

    #### 6-2. Sitemap
    - 使用 [draw.io](https://www.draw.io/) 畫出 sitemap，包含所有頁面，若頁面需呈現的資訊比較多，也可以列出該頁內容

### 7. 討論項目
- 列出內部需與客戶確認的事項，並於會議中確認
- 若未經過提案階段，通常會於此次會議和客戶討論 reference 並請客戶選擇偏好的風格

    #### reference
    - 根據 sales 提供的參考資料或描述在下列網站搜尋適合的參考：[Awwwards](https://www.designawards.asia/)、[DesignAwards.Asia](https://www.designawards.asia/)、[bm.s5-style](http://bm.s5-style.com/)
    - 亦可直接搜尋相近產業品牌網站

### 8. 臨時動議
- 詢問客戶是否有其他項目需討論


## 客戶常見問題

## 其他提醒項目
- Kick-off Meeting 為 PM 首次與客戶見面，務必要帶名片，同時需注意服裝及禮節

## 會議紀錄
會議後需根據討論結果整理[會議記錄](客戶相關會議/會議記錄)後寄出