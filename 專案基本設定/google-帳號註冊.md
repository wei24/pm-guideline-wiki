帳號：25sprout +`客戶名稱`@gmail.com  
密碼：`客戶名稱` + 新芽統編

### 帳號基本資料
姓：25sprout  
名：客戶名稱 
  

### 綁卡設定
- https://console.cloud.google.com/billing
- 新增帳單帳戶  
![image](uploads/0fc78edf7e52e2b35677aa20d28de0f0/image.png)  
- 同意條款
- 輸入帳戶資料及信用卡資訊  
    - 輸入公司統編 `53754077`、正式名稱 `新芽網路股份有限公司` 與地址
    - 需更改主要聯絡人資訊為自己、e-mail 請填寫 `pmxacct@25sprout.com`，這樣如果有扣款，財務跟 PM 都可以收到通知
    - 付款方式請跟財務拿公司的信用卡填寫  
![image](uploads/e4a4d43f05fa5307137269c9744e060c/image.png)  
![image](uploads/40091db21f4c02dfec0723ce382bacb5/image.png)
- E-mail 會收到一封要求你同意的信件，務必點選才可以正確收到相關的帳單資訊  
![image](uploads/a50dc0281c25cb8099d2c712a520bd28/image.png)