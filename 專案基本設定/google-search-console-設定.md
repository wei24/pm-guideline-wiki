## 使用情境
每次專案上線後，需使用為客戶建立的 Google 帳號進行以下提交作業。若客戶希望透過自己的帳號提交，需請客戶開權限或告知客戶要如何進行。

#### 驗證擁有權
- 在進行相關設定前，Google 會先確認網站為此帳號所有
- 驗證方式如下，通常會採用第一點  
  ![image](uploads/e25833320d9a1f6203abcc2e882ae878/image.png)

#### 新增 sitemap
- 請開發者產出 sitemap 後上傳至網站根目錄，並提供網址給 PM
- PM 需於以下介面提交 sitemap 網址給 Google
  ![image](uploads/1b2b857d543dfd6b34fa475353a706be/image.png)
- 參考網址：https://search.google.com/u/2/search-console/sitemaps?resource_id=http%3A%2F%2Fwww.25sprout.com%2F

#### 使用 Google 模擬器確認網站可以被 Google 爬到
- Google 官方對此功能的[說明](https://support.google.com/webmasters/answer/6066467?hl=zh-TW)
- 參考網址：https://www.google.com/webmasters/tools/googlebot-fetch?hl=zh-TW&authuser=2&siteUrl=http://www.25sprout.com/
- 模擬時選擇 `檢索並翻譯`

