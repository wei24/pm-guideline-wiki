Meta 資料設置於 header 內，通常為全站共用  
若需動態由後台設定，前端需另外設定用 php 

### Title
顯示於瀏覽器頁籤、搜尋結果，通常為網站名稱  
![image](https://fox.25sprout.com/pm/draft/uploads/b924ad934da0c8334a899faffcbf8c31/image.png)
![image](https://fox.25sprout.com/pm/draft/uploads/226b99554f2fbd35c5698a88b6122af1/image.png)
### Description
顯示於搜尋結果的說明文字
![image](https://fox.25sprout.com/pm/draft/uploads/226b99554f2fbd35c5698a88b6122af1/image.png)

### Keywords
主要用途為給 Google 爬的關鍵字

### Favicon
顯示於瀏覽器頁籤、搜尋結果的縮圖，通常使用 .ico 格式圖檔
![image](https://fox.25sprout.com/pm/draft/uploads/b924ad934da0c8334a899faffcbf8c31/image.png)

### FB 
[官方說明](https://developers.facebook.com/docs/sharing/webmasters?locale=zh_TW)
##### 常用 Tag 
- og:title：通常與 meta title 相同
- og:description：通常與 meta description 相同
- og:image：FB 分享時會顯示的圖片，建議尺寸為 1200*628
- og:url：網站網址
##### [分享偵錯工具](https://developers.facebook.com/tools/debug/)
主動提交給 Facebook 重新爬資料的工具，通常用於有更換相關資料後，可以加速在 Facebook 顯示新的網站資料