## 前言
- 所有與主機、DNS、FTP、資料庫相關的設定皆需在兩個地方開 issue
  1. 專案 GitLab 開立 issue 供追蹤時程使用，issue 範例 https://fox.25sprout.com/25sprout-project/template/PM_template/-/issues/10、https://fox.25sprout.com/25sprout-project/template/PM_template/-/issues/11
  2. servers-wall 開立 issue 描述詳細 spec，並作為後續主機管理使用。基本上一台主機一個 issue ，關於該主機的所有設定皆寫在同一個 issue 內。Servers-wall issue 內容說明如下：


## 主機開立
- 主機申請 issue 範本  
    - 一般主機 https://fox.25sprout.com:10088/25sprout/servers-wall/issues/94  
    - AWS https://fox.25sprout.com:10088/25sprout/servers-wall/issues/95  
    - issue 標題範例： `雲朗觀光 / 兆品酒店官網正式機`，需標明客戶、專案名稱、測試機/Staging/正式機

### 測試機/staging 機
1. 於 25sprout/servers-wall> 根據範本新增 issue，label 需選擇客戶及專案  
2. 請與後端開發者確認，BackStage 與 PHP 版本 
3. 可以將 due date 設定為希望 server master 設定好的時間，基本上設定為開始需要主機資訊/開發的 3-5 個工作天前，Server Master 通常可以在開完 issue 隔天起 3-5 個工作天內完成，若有急件請另外跟 Server Master 討論
4. 將 issue assign 給 server master
5. server master 開完主機會將 CI 設定好後 reassign 給 PM
6. 若為舊專案或需使用 ftp 上傳， server master 提供主機資訊後，由 PM 私訊給開發者，一律不存在 GitLab

### 正式機
1. 於 25sprout/servers-wall> 新增 issue，label 需選擇客戶及專案  
2. 請與後端開發者確認，BackStage 與 PHP 版本 
3. 若為客戶自有主機需私訊提供開發者帳號、密碼等相關資訊
4. 若為新 issue 可以將 due date 設定為希望 server master 設定好的時間，基本上設定為開始需要主機資訊/開發的 3-5 個工作天前，若有急件請另外跟 Server Master 討論。既有 issue 的話，該欄位為主機租約到期日，不可變動，可將希望設定完的時間寫在 comment
5. 將 issue assign 給 server master
6. server master 開完主機會將 CI 設定好後 reassign 給 PM
7. 若為舊專案或需使用 ftp 上傳， server master 提供主機資訊後，由 PM 私訊給開發者，一律不存在 GitLab

## 網域設定
1. 若為 issue 開立後追加，於該主機 issue 內新增項目並在 issue 留言
2. 新增完後將 issue assign 給 server master
3. server master 設定完會 reassign 給 PM

## FTP 帳號/資料庫設定  
1. 通常為客戶有 FTP 帳號/資料庫需求
2. 若為 issue 開立後追加，於該主機 issue 內新增項目並在 issue 留言
3. FTP 需提供帳號及該帳號可以進入的資料夾路徑，範例如下：
```
路徑：seiko/isseymiyake
帳號：seiko-im
```
4. 新增完後將 issue assign 給 server master
5. server master 新增完會 reassign 給 PM，並私訊 PM ftp 資訊
6. 主機資訊由 PM 私訊給開發者，一律不存在 GitLab


## VPN 設定  
1. 通常為客戶主機有擋 IP，有外部人員需要透過新芽的 IP `122.116.66.184` 去連客戶主機
2. 若為 issue 開立後追加，於該主機 issue 內新增項目並在 issue 留言
3. 新增完後將 issue assign 給 server master
4. server master 設定完會 reassign 給 PM