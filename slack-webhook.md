## 開立新的 Slack

- 每個新的專案皆需開新的 channel
  - channel 命名規則：`客戶名稱_專案名稱`，例如 `allianz_portal`，如為包含外部開發者或客戶的 channel，命名方式為加上 `_partners`，例如：`allianz_portal_partners`，並提供 e-mail 請 PM head 新增使用者至該 channel。若前端或後端其中一個部門只有外部開發者，只要使用原本的 channel 即可，不用另外開給外部的 channel。
  - channel 權限：Private  
![image_-_2020-08-25T103723.025](uploads/4aeed4f97275dfb24253e5428173ba6f/image_-_2020-08-25T103723.025.png)
  - 每個 channel 除了專案成員外，另需加入 Alex、Thundersha、前端開發經理、後端開發經理、PM Head、Server Master（Suhu）、Cloud Master（Peter）


## 建立 Slack 與 Gitlab 專案的 Webhook

##### 前言
25sprout 使用 Slack 作為內部訊息溝通軟體，GitLab 作為程式碼管理、版本控制和專案管理工具。  
每個 GitLab 專案應都有對應的 Slack Private Group，透過 Webhook 可以將 GitLab 的更新訊息自動通知到 Slack，減少人員溝通的需求並可以有效地追蹤進度。  
本文簡述如何設定 Slack 與 GitLab 之間的 Webhook 設定。

### 懶人包設定流程
1.    直接在 Slack 登入狀態下，點擊以下網址：https://my.slack.com/services/new/incoming-webhook/
1.    選擇 `Private Group` 名稱。
1.    完成後找到 `Webhook URL`，將 URL 複製下來。
1.    回到 GitLab，從 GitLab 專案左側點擊設定的圖示，進入 `Integrations`。
1.    找到最下面 `Slack Notifications`後點擊進入。
1.    將第一個 Checkbox `Active` 打勾。
1.    將剛剛複製下來的 `Webhook URL` 貼到最下面的 `Webhook`，如下圖：
![image_-_2020-08-25T103751.429](uploads/9926a31073837842f6a55be9b174acb1/image_-_2020-08-25T103751.429.png)
1.    並將下方的 Username 設定為專案名稱
![image_-_2020-08-25T103755.239](uploads/d26e937b82b0e70cc8d3c5ecf433450c/image_-_2020-08-25T103755.239.png)
1.    完成，接下來所有在 GitLab 的動作都會自動傳送到 Slack 囉

### 多個 Slack channel 與 GitLab project 的對應
1. 當 Slack 有分為給外部開發者及內部開發者的 channel 時，該專案的 PM project 要串接到哪一個，基本上根據該專案主要開發者為外部或內部。例如前端主力開發者為外部人員，則將 PM project 串到外部的 channel。
2. 與上程式碼相關的通知，開發者基本上會自行設定。若分為內外部通常會設在外部的，以讓所有開發者都看到程式碼 commit 的結果。
3. 若一段時間後專案仍在進行中，但外包開發者負責的工作已告一段落且 issue 也都交付測試無誤後，即可將外部開發者自 partners 的 channel 中移除。

## Slack 與 Zeplin 串接

- Zeplin：建議至網頁版 Zeplin 設定，通常可以不用再登入一次 Slack  
    - 至 Zeplin project 右側選擇 Integrations: Add to Slack   
![螢幕快照_2018-10-04_下午10.37.28](uploads/1c5b855d30a0ba528e60ff14c0106af7/螢幕快照_2018-10-04_下午10.37.28.png)
    - 選擇對應 project 並點下 Authorize  
![螢幕快照_2018-10-04_下午10.33.18](uploads/e39a414eef2c3f15390e4957b691af40/螢幕快照_2018-10-04_下午10.33.18.png)
    - 完成設定可於 Zeplin project 看到連結的 Slack channel、Slack 亦會顯示新增 integration
![螢幕快照_2018-10-04_下午10.35.16](uploads/2be08ae19b4ac4d92db5bf966df76784/螢幕快照_2018-10-04_下午10.35.16.png)  
![螢幕快照_2018-10-04_下午10.36.31](uploads/686c1bcf6aac50bbae82a0ea725cb710/螢幕快照_2018-10-04_下午10.36.31.png)