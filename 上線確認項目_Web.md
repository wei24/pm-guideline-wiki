#### 上線前
- [ ] 開正式機環境
- [ ] [正式 meta 設定](https://fox.25sprout.com/25sprout-project/guidelines/-/wikis/%E5%B0%88%E6%A1%88%E5%9F%BA%E6%9C%AC%E8%A8%AD%E5%AE%9A/Web-meta-information)
- [ ] 全站一組 GA 或 GTM：請客戶提供
- [ ] 全站一組 FB Pixel：請客戶提供 （Optional，通常有 GTM 可能就不會額外設定 Pixel）
- [ ] 確認該次上線功能都已完成
- [ ] 前後端開發者更新至正式機
- [ ] 確認該次上線功能在正式機正確運作：用 host 設定 local 網域指向
- [ ] DNS 轉址設定：客戶或新芽 MIS 進行

#### 上線後
- [ ] [Google Search Console 設定](https://fox.25sprout.com/25sprout-project/guidelines/-/wikis/%E5%B0%88%E6%A1%88%E5%9F%BA%E6%9C%AC%E8%A8%AD%E5%AE%9A/google-search-console-%E8%A8%AD%E5%AE%9A)