### 為什麼需要更改 HOST 設定
- DNS 還沒指到新主機、或 DNS 設定尚未生效前，可以透過更改 host 設定，讓你的電腦連到該網域時，可以連到新的主機
- 常見使用情境可能是客戶原有官網轉到新官網的過渡期，可以透過這個方式看上到正式機的資料是不是正常運作

### Mac 系統設定方式
- 請先跟 Server Master 取得該主機的 IP
- 步驟
    1. 打開終端機  
    ![image](uploads/fa231826da868a8302ee621894df8fd8/image.png)
    2. 輸入 `sudo vi /private/etc/hosts`
    3. 輸入電腦密碼
    4. `e` 進入編輯
    4. `i` 開始輸入資料
    5. 在最下方新增要設定的指向，前面是 IP、後面是網域。例如：33.634.234.22  www.mitravel.com
    6. 按下 esc -> 輸入 `:wq` (表示儲存並離開)，亦可輸入 `:w` （表示儲存）
    7. 設定就生效啦~在手機
- 參考文章：https://www.kocpc.com.tw/archives/6527
- DNS 正式生效後務必移除，避免後續的測試不準確

