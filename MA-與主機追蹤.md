## 說明
主機費及維護費為網站上線後每年由 PM 追蹤的固定收入，PM 須於下列時間點一週內填寫紀錄，往後每年收到 GitLab 到期通知信後一週內開立報價單及專案保固書，進入報價審核流程。

## 流程
1. 主機：專案首次上線時，填寫[上線紀錄](https://25sprout.surveycake.com/s/Kp0dA)，`專案是否已經完整上線` 選擇 `否`，紀錄主機啟用時間。並[E-mail 通知](https://fox.25sprout.com/pm/template/issues/1)客戶已完成上線，開始起算主機租用時間。
2. MA：專案若分階段上線，於專案全數上線時開始計算保固。全站完整上正式機時需 [E-mail 通知](https://fox.25sprout.com/pm/template/issues/1)客戶已上線，且保固也從當天起算，並填寫[上線紀錄](https://25sprout.surveycake.com/s/Kp0dA)，`專案是否已經完整上線` 選擇 `是，已填寫主機啟用紀錄`紀錄保固開始時間。
3. 專案全部功能同時上線需 [E-mail 通知](https://fox.25sprout.com/pm/template/issues/1)客戶已上線，而主機跟保固也從當天起算，並填寫[上線紀錄](https://25sprout.surveycake.com/s/Kp0dA)，`專案是否已經完整上線` 選擇 `是`，同時紀錄主機及保固起算時間。
4. 確認 Issue 有正確開立於 Project Wall，並加上客戶 label

#### 手動建立 Issue 及 Google Sheet 資料
若問卷無法運作，需手動建立以下追蹤資訊
1. Project Wall Issue  
- Issue 範本
    - 主機：https://fox.25sprout.com:10088/25sprout/projects-wall/issues/299
    - MA：https://fox.25sprout.com:10088/25sprout/projects-wall/issues/300
- Assignee：PM  
- Due Date：`專案首次上線日` 或 `專案完成日` +10 個月  
- Label：設為 `主機` 或 `MA` + `客戶名稱`  

2. 新增 Google Sheet 資料
- 資料範例
    - 主機：https://docs.google.com/spreadsheets/d/13l9FCJX06Jf6deVpYM0-2m5-o78SIPz3hBiPb5TEqVU/edit#gid=1231821173&range=19:19
    - MA：https://docs.google.com/spreadsheets/d/13l9FCJX06Jf6deVpYM0-2m5-o78SIPz3hBiPb5TEqVU/edit#gid=358837641&range=10:10


## 每年
- PM 於收到 GitLab 到期通知信後一週內開立報價單，進入報價審核流程。MA 除報價單外，亦須提供保固書。
    - 請下載[保固書範本](https://drive.google.com/open?id=1BmWyrCsLXEUW4PrJDHSGEBeMt6zGGjVY)，並根據[保固書說明文件](https://docs.google.com/document/d/1CcTkrYtEhR-hWGyb1rWsSJxaF4e1_v-zv9GN-VCAeLw/edit?usp=sharing)調整，未避免字體及其他樣式跑版，請勿使用 Google Doc 檔案編輯
    - [MA 報價單範本](https://docs.google.com/spreadsheets/d/1zY0vkXzyw28aIQpO_y1hUW-UfPA16acXki2gFwnmKKo/edit#gid=307345390)：保固期間與保固項目請依照保固書內容更新，並將黃色 highlight 移除。贈送人天為 MA 金額每 35,000 元（未稅），贈送 1 個人天，贈送最小單位為 0.5 個人天，例如 52,500 元則贈送 1.5 個人天。若專案合約有列贈送人天，則以合約為主。
- 主機：需截圖紀錄前一年度 12 個月的流量（包含主機流量 `awstats` 及 CDN 流量 `CF Metrics`），若每月有超過合約的規格，需提出建議客戶升級，升級規格需參照[報價的區間](https://fox.25sprout.com:10088/25sprout/projects-wall/-/wikis/vps-quotation)，例如平均使用約 650G，則建議客戶升至 750G。
    - 流量統整表格示意  
![image](uploads/1c8cdc56370fb20eed992a525f9905d2/image.png)
- 每年收到客戶回簽的報價單後，需關閉當年的 issue，並複製該筆資料的 GitLab issue / Google Sheet 內容後，各別新增下一個年度的，更新內容包含 GitLab issue due date 及 Google Sheet 金額（若與前一年有差異）、預計開發票日期。
- 若未簽合約或合約內未提到 MA 的客戶，須於主機到期時詢問是否加入 MA。MA 說明內容 https://fox.25sprout.com/pm/template/issues/4


## 每季
- 主機：每季第一個月（1, 4, 7, 10）需至 servers-wall 各專案 issue 內的 awstats 頁面，確認前一季各站主機流量，並截圖紀錄於 projects-wall 主機追蹤的 issue，若每月流量超過合約的規格，需提出建議客戶升級，升級規格需參照[報價的區間](https://fox.25sprout.com:10088/25sprout/projects-wall/-/wikis/vps-quotation)，例如平均使用約 650G，則建議客戶升至 750G。