[[_TOC_]]

# 25sprout Project Management Guideline
- [各職位與主要窗口轉換](https://25sprout-project.fox.25sprout.com/guidelines/position.html)
- [一般專案流程](https://25sprout-project.fox.25sprout.com/guidelines/) 
- SurveyCake 專案相關流程
    - [SurveyCake Sales & Solution PM flow](https://25sprout-project.fox.25sprout.com/guidelines/Solution_PM_SurveyCake.html)
    - [SurveyCake Automation 專案流程](https://25sprout-project.fox.25sprout.com/guidelines/SurveyCake_Automation.html)
    - [SurveyCake 客製化需求流程](https://25sprout-project.fox.25sprout.com/guidelines/SurveyCake_Customization.html)


# 專案流程項目&文件說明
## 報價
- [專案報價](/文件/專案報價)

## Kick-off
### 文件
- [SOW](/文件/sow-工作說明書)
- [時程表](/文件/時程表)
- [GitLab Projects-wall Issue 介紹與需設定的項目](https://fox.25sprout.com/25sprout/projects-wall/wikis/projects-wall-intro)
- [Slack 建立與 Webhook 設定](https://fox.25sprout.com/25sprout/projects-wall/wikis/Slack-%E5%BB%BA%E7%AB%8B%E8%88%87-Webhook-%E8%A8%AD%E5%AE%9A)
### 會議
- [與客戶的 Kick-off Meeting](/客戶相關會議/kick-off-meeting)
- [內部 Kick-off Meeting](/內部會議/內部-kick-off-meeting)
- [客戶例會與會議記錄](/客戶相關會議/客戶例會)
- [內部例會](/內部會議/內部例會)
- [會議記錄](/客戶相關會議/會議記錄)
- [Calendar 會議事件管理](/Calendar-會議事件管理)
- [提案簡報參考](https://drive.google.com/drive/folders/1e21Fb8kBJvl1kb62P7PWotm4_iGN2ekb)
### 專案管理相關軟體設定
- [GitLab 設定與使用說明、各類 Issue 說明](/專案管理工具/gitlab-使用說明)
- [Slack 設定](https://fox.25sprout.com/25sprout/projects-wall/wikis/Slack-%E5%BB%BA%E7%AB%8B%E8%88%87-Webhook-%E8%A8%AD%E5%AE%9A)
- [Google Drive 設定](/專案管理工具/google-drive-與文件管理說明)   
### 外包專案
- [開發規格書](/文件/外包相關文件#開發規格書)
- [外包合約](/文件/外包相關文件#合約)
- [外包 NDA](/文件/外包相關文件#nda) 

## 設計
- [Flow Chart](/規格文件/flow-chart)
- [Wireframe]
 

## 開發
- [Feature Issue 範例與說明](https://fox.25sprout.com/25sprout-project/template/PM_template/issues/1)

## 測試
- [測試概要](/測試概要)
- [APP 打包&測試流程及注意事項](/APP 打包&測試流程及注意事項)
- [Bug Issue 範例與說明](https://fox.25sprout.com/25sprout-project/template/PM_template/issues/3) 

## 驗收
### [APP 上架所需文件](/文件/app-上架文件)
### [上線確認項目_Web](/上線確認項目_Web)
### [文案範本](/文案範本)
- 需要替客戶上稿的專案可以提供此範本給客戶，提升上稿資料易讀性及上稿效率
### 可能的驗收文件
- [測試計畫](驗收文件/測試計畫)  
- [驗收單](驗收文件/驗收單)  
- [測試報告](驗收文件/測試報告)  
- [操作手冊](驗收文件/操作手冊)  


## 財務
- 報價單成立與開立發票
    - [財務說明](https://fox.25sprout.com/25sprout/projects-wall/wikis/quotation)
    - SurveyCake 表單：[銷售流程 – 訂單建立 & 發票開立](https://www.surveycake.com/s/aYzmp)
- [非銷售相關的文件用印](https://fox.25sprout.com/25sprout/projects-wall/-/wikis/%E6%96%87%E4%BB%B6%E7%94%A8%E5%8D%B0%E8%88%87%E9%83%B5%E5%AF%84)
- [建立採購單與付款](https://fox.25sprout.com/25sprout/projects-wall/wikis/procurement)
 
## Infra 環境設定
- [主機 / 網域（DNS）/ FTP 等 Infra 環境設定](/主機、網域、FTP-帳號相關設定)
- [SSL CSR 設定所需資料](https://fox.25sprout.com/25sprout/servers-wall/wikis/ssl/gen-csr)


# 專案基本設定
包含 Google 相關服務、SEO 相關等設定
#### [Web meta 設定](/專案基本設定/Web-meta-information)：Meta 設定會影響頁籤顯示的網站名稱、分享至社群顯示的資訊、以及 Google 搜尋引擎會爬到的資料
#### [Google Search Console 設定](/專案基本設定/google-search-console-設定)：提交 sitemap
#### [Googel 帳號註冊](/專案基本設定/google-帳號註冊)：使用 Google 註冊/登入、Google Map 的專案都需要申請

# Incident Management 
- [突發事件處理辦法](Incident Management)
    - [突發事件檢討報告](https://fox.25sprout.com/km/incident_management/-/issues)
   
# Other Resources
- [知識庫](https://fox.25sprout.com/km)
    - [每月 workshop 簡報/外部網站 reference 紀錄](https://fox.25sprout.com/km/workshop)
    - [C&S 特殊事件情境與紀錄](https://fox.25sprout.com/km/casestudy)
- [SurveyCake public document](https://fox.25sprout.com/surveycake/public-document/-/blob/master/README.md)

# 微技術
個別專案偶爾會需要使用
#### [更改電腦 Host 設定 - Mac OS](/host-設定) 
#### [GitLab Pages 使用](https://fox.25sprout.com/25sprout-project/shared-doc/wikis/pages%E4%BD%BF%E7%94%A8%E6%96%B9%E5%BC%8F) 
#### [弱掃報告怎麼看](https://fox.25sprout.com/25sprout/projects-wall/wikis/%E5%BC%B1%E6%8E%83%E5%A0%B1%E5%91%8A%E6%80%8E%E9%BA%BC%E7%9C%8B%EF%BC%9F)
#### [主機常見問題 by 環境委員會](https://fox.25sprout.com/25sprout/cloud-manager/pm_routine)


# PM 日常

## 會議

* [PM 例行會議列表與說明](https://fox.25sprout.com/pm/dailyadmin/wikis/%E6%96%B0%E8%8A%BD-pm-%E4%BE%8B%E8%A1%8C%E6%9C%83%E8%AD%B0)

## 信件

* [各式信件範本](https://fox.25sprout.com/pm/dailyadmin/wikis/%E4%BF%A1%E4%BB%B6%E7%AF%84%E6%9C%AC)

## [休假與 Remote](https://fox.25sprout.com/pm/dailyadmin/wikis/home#%E4%BC%91%E5%81%87%E8%88%87-remote)


## 其他

* [專案 PM 轉換交接流程](PM-轉換交接)